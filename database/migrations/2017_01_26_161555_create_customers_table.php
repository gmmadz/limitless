<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last_name');
            $table->string('first_name');
            $table->date('join_date');
            $table->boolean('status');
            $table->date('current_joined_date');
            $table->string('membership_type');
            $table->string('notes');
            $table->string('address');
            $table->string('mobile_number');
            $table->string('email');
            $table->string('gender');
            $table->date('date_of_birth');
            $table->string('emergency_contact_person');
            $table->string('emergency_contact_number');
            $table->longText('difficulties_injuries');
            $table->longText('goals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Tuition;
use App\Transaction;
use App\Dropin;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function report(Request $request){
        $tuitions = Tuition::whereBetween('date', array($request->start." 00:00:00", $request->end." 23:59:59"))->get();
        $dropins = Dropin::whereBetween('date', array($request->start." 00:00:00", $request->end." 23:59:59"))->get();
        $transactions = Transaction::whereBetween('date', array($request->start." 00:00:00", $request->end." 23:59:59"))->get();


        return view('results')->withTransactions($transactions)->withDropins($dropins)->withTuitions($tuitions)->withFrom($request->start)->withTo($request->end);
    }

    public function logout()
    {
        Auth::logout();
        return view('auth.login');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tuition;
use App\Customer;

class TuitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tuitions = Tuition::all();
        $fullname = Customer::get()->pluck('full_name', 'id');

        return view('tuition')->withTuitions($tuitions)->withFullname($fullname);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tuition = new Tuition;
        $payment = str_replace(',', '', $request->payment);

        $tuition->customer_id = $request->customer_id;
        $tuition->type = $request->type;
        $tuition->date = $request->date;
        $tuition->payment = $payment;

        $tuition->save();

        $customer = Customer::find($request->customer_id);

        $customer->membership_type = $request->type;
        $customer->current_joined_date = $request->date;

        $customer->save();

        return redirect()->route('tuitions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $customer = Customer::find($id);
        $tuitions = Tuition::where('customer_id','=',$id);
        $tuition = Tuition::find($request->tid);

        if($tuition->count() > 1){
            $tuition->delete();
            $customer->current_joined_date = uclast($tuitions->date);
        }

        return redirect()->route('tuitions.index');
    }
}

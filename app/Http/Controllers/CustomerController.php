<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Tuition;
use App\Transaction;
use App\Dropin;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('members')->withCustomers($customers);
    }

    public function profile($id)
    {
        $customer = Customer::findOrFail($id);
        return view('profile')->withCustomer($customer);
    }

    public function deleteAll($id){
        $transactions = Transaction::where('customer_id','=',$id)->delete();

        $tuitions = Tuition::where('customer_id','=',$id)->delete();

        $dropins = Dropin::where('customer_id','=',$id)->delete();

        $customer = Customer::findOrFail($id);
        $customer->delete();
        
        $customers = Customer::all();
        return view('members')->withCustomers($customers);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer;

        //provided
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->gender = $request->gender;
        $customer->address = $request->address;
        $customer->mobile_number = $request->phone_no;
        $customer->email = $request->email;
        $customer->membership_type = $request->membership;
        $customer->difficulties_injuries = $request->d_i;
        $customer->notes = $request->notes;
        $customer->goals = $request->goals;
        $customer->emergency_contact_person = $request->emergency_contact_person;
        $customer->emergency_contact_number = $request->emergency_contact_number;
        $customer->date_of_birth = $request->date_of_birth;

        //auto
        $customer->join_date = $request->date;
        $customer->status = 1;
        $customer->current_joined_date = $request->date;

        $customer->save();

        //tuition table
        $tuition = new Tuition;
        $payment = str_replace(',', '', $request->payment);

        $tuition->customer_id = $customer->id;
        $tuition->type = $request->membership;
        $tuition->date = $request->date;
        $tuition->payment = $payment;

        $tuition->save();

        return redirect()->route('customers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);

        //provided
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->gender = $request->gender;
        $customer->address = $request->address;
        $customer->mobile_number = $request->phone_no;
        $customer->email = $request->email;
        $customer->difficulties_injuries = $request->d_i;
        $customer->notes = $request->notes;
        $customer->goals = $request->goals;
        $customer->emergency_contact_person = $request->emergency_contact_person;
        $customer->emergency_contact_number = $request->emergency_contact_number;
        $customer->date_of_birth = $request->date_of_birth;

        $customer->save();

        return redirect()->route('customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

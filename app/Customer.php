<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function getFullNameAttribute(){
    	return ucfirst($this->last_name) . ', ' . ucfirst($this->first_name);
    }

    public function transactions(){
    	return $this->hasMany('App\Transaction');
    }

    public function tuitions(){
    	return $this->hasMany('App\Tuition');
    }

    public function dropins(){
    	return $this->hasMany('App\Dropin');
    }
}

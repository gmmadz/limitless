<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$customers = App\Customer::all()->count();
	$dropins = App\Dropin::all()->count();
	$rents = App\Transaction::where('type','=','Rent')->whereMonth('date','=',date('m'))->count();
	$purchases = App\Transaction::where('type','=','Purchase')->whereMonth('date','=',date('m'))->count();
    return view('welcome')->withCustomers($customers)->withRents($rents)->withPurchases($purchases)->withDropins($dropins);
});

Route::get('reports', function () {
    return view('reports');
});

Route::post('reports', 'HomeController@report');

Route::get('/customers/{id}', 'CustomerController@profile');

Route::post('/delete/{id}', 'CustomerController@deleteAll');

Route::resource('customers', 'CustomerController', ['except' => ['create', 'show']]);

Route::resource('transactions', 'TransactionController');

Route::resource('tuitions', 'TuitionController');

Route::resource('dropins', 'DropinController');

Route::resource('tuitions', 'TuitionController');

Route::get('/logout', 'HomeController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index');

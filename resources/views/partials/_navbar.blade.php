<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/~gmmadz//home">Limitless, @if (Auth::guest()) Guest @else 
        {{ Auth::user()->name }} @endif</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <a class="navbar-brand" href="" data-toggle="modal" data-target="#about">
                    About
        </a>
        <!-- /.dropdown -->
        <li class="dropdown">
        @if (!(Auth::guest()))
            
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>

                
                <ul class="dropdown-menu dropdown-user">
                    <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i>Create New Admin</a>
                    </li>
                    <li class="divider"></li> -->
                    <li><a href="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
                
            
        @endif
        </li>
            <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
    @include('partials._sidebar')
</nav>




<div class="modal fade" id="about" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">About</h3>
    </div>
    <div class="modal-body">

       <b>Members:</b>

       <ol>
        <li> Enrique Jr., Roland D. </li>
        <li> Filosopo, Mary Joahnne P. </li>
        <li> Madarang, Gerald Martin C.</li>
       </ol>

       <b>About the Project:</b> </br>
       Limitless Fitness and Martial Arts is a fitness center located in Davao City. This site aims to store members' records and track their drop-ins and transactions. It also has summary reports based on the selected time frame.

       </br></br>
       <b>Sample Account:</b> </br>
       E-mail: johndoe.addu.edu.ph </br>
       Password: cs.addu


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top:20px">Close</button>
        
    </div>
   
</div>
</div>
</div>

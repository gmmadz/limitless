<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="/~gmmadz"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="customers"><i class="fa fa-users fa-fw"></i> Members</a>
            </li>
            <li>
                <a href="dropins"><i class="fa fa-child fa-fw"></i> Drop-Ins</a>
            </li>
            <li>
                <a href="transactions"><i class="fa fa-shopping-cart  fa-fw"></i> Rents & Purchases</a>
            </li>
            <li>
                <a href="tuitions"><i class="fa fa-money fa-fw"></i> Tuitions</a>
            </li>
            <li>
                <a href="reports"><i class="fa fa-file-text fa-fw"></i> Reports</a>
            </li>
            <!-- <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="blank.html">Blank Page</a>
                    </li>
                    <li>
                        <a href="login.html">Login Page</a>
                    </li>
                </ul>
            </li> -->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
            <!-- /.navbar-static-side -->

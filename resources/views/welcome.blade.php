<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials._head')
</head>

<body>

    <div id="wrapper">

    @include('partials._navbar')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{date('l - M d, Y', strtotime(\Carbon\Carbon::now()))}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            @include('partials._summary')
        </div>
    </div>
    @include('partials._scripts')
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials._head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
</head>

<body>

    <div id="wrapper">

        @include('partials._navbar')

        <div id="page-wrapper" style="overflow: auto;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Members @if (Auth::guest()) <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCustomer" disabled> @else <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCustomer"> @endif
                      <i class="fa fa-plus fa-fw"></i><i class="fa fa-user fa-fw"></i>
                  </button>
              </h1>
          </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Membership</th>
                                <th>Status</th>
                                <th>Expiration</th>
                                <th>Days Left</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                            <tr class='clickable-row'data-href="/customers/{{$customer->id}}">
                                
                                <td>{{$customer->first_name}} {{$customer->last_name}}</td>
                                <td>{{$customer->gender}}</td>
                                <td>{{$customer->membership_type}}</td>
                                <?php 
                                    $date1 = new DateTime();
                                    $date2 = new DateTime($customer->current_joined_date. '+30 days');

                                    $diff = $date2->diff($date1)->format("%a");
                                ?>
                                <td>
                                @if($date1 == $date2)
                                    <span class="label label-default">Inactive</span>
                                @elseif($date1 > $date2)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-success">Active</span>
                                @endif
                                </td>
                                <td>
                                {{$date2->format("M d, Y")}}
                                </td>
                                <td>
                                    <span class="label label-info">
                                @if($date1 == $date2)
                                    Expired <b>{{$diff}}</b> day/s ago.
                                @elseif($date1 > $date2)
                                    <b>Expires Today!</b>
                                @else
                                    Expires in <b>{{$diff}}</b> day/s.
                                @endif
                                </span>
                                </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="profile<?php echo $customer->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h3 class="modal-title" id="myModalLabel">Member Profile - {{$customer->first_name}} {{$customer->last_name}}</h3>
                                        </div>
                                        <div class="modal-body">
                                            <dl>
                                                <h3>Member Info</h3>
                                                <dt>Full Name</dt>
                                                <dd>{{$customer->first_name}} {{$customer->last_name}}</dd>

                                                <dt>Gender</dt>
                                                <dd>{{$customer->gender}}</dd>

                                                <dt>Date of Birth</dt>
                                                <dd>{{$customer->date_of_birth}}</dd>

                                                <br>
                                                <h3>Membership Details</h3>
                                                <dt>Original Joined Date</dt>
                                                <dd>{{$customer->join_date}}</dd>

                                                <dt>Current Joined Date</dt>
                                                <dd>{{$customer->current_joined_date}}</dd>

                                                <dt>Status</dt>
                                                <dd>{{$customer->status}}</dd>

                                                <dt>Membership Type</dt>
                                                <dd>{{$customer->membership_type}}</dd>

                                                <dt>Notes</dt>
                                                <dd>{{$customer->notes}}</dd>

                                                <dt>Goals</dt>
                                                <dd>{{$customer->goals}}</dd>

                                                <dt>Difficulties/Injuries</dt>
                                                <dd>{{$customer->difficulties_injuries}}</dd>

                                                <br>
                                                <h3>Emergency Info</h3>
                                                <dt>Contact Person</dt>
                                                <dd>{{$customer->emergency_contact_person}}</dd>

                                                <dt>Contact No.</dt>
                                                <dd>{{$customer->emergency_contact_number}}</dd>
                                            </dl>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
</div>

<!-- Add Customer Modal -->
<div class="modal fade" id="addCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Add New Member</h3>
    </div>
    <div class="modal-body">

        <h4>Member Info</h4>
        <hr>
        {!! Form::open([]) !!}
        <input type="hidden" name="date" value="{{\Carbon\Carbon::now()}}"/>

        <!-- Last Name -->
        {!! Form::label('last_name', 'Last Name:') !!}
        {!! Form::text('last_name', null, array('class' => 'form-control', 'required' => '')) !!}
        <!-- First Name -->
        {!! Form::label('first_name', 'First Name:') !!}
        {!! Form::text('first_name', null, array('class' => 'form-control', 'required' => '')) !!}
        <!-- Gender -->
        {!! Form::label('Gender', 'Gender:') !!}
        {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['placeholder' => 'Gender...', 'class' => 'form-control']) !!}
        <!-- Date of Birth -->
        {!! Form::label('last_name', 'Birthdate:') !!}
        {!! Form::text('date_of_birth', null, array('id' => 'datepicker', 'class' => 'form-control')) !!}

        <!-- Address -->
        {!! Form::label('address', 'Address:') !!}
        {!! Form::text('address', null, array('class' => 'form-control', 'required' => '')) !!}
        <!-- Phone No. -->
        {!! Form::label('phone_no', 'Mobile No.:') !!}
        {!! Form::text('phone_no', null, array('class' => 'form-control', 'required' => '')) !!}
        <!-- Email -->
        {!! Form::label('email', 'E-Mail:') !!}
        {!! Form::text('email', null, array('class' => 'form-control', 'required' => '')) !!}


        <br><br>
        <h4>Membership Details</h4>
        <hr>
        <!-- Membership Type -->
        {!! Form::label('Membership', 'Membership') !!}
        {!! Form::select('membership', ['Single' => 'Single Membership', 'Dual' => 'Dual Membership', 'Triple' => 'Triple Membership', 'Quad' => 'Quad Membership'], null, ['placeholder' => 'Membership Type...', 'class' => 'form-control', 'requried' => '']) !!}
        <!-- Payment -->
        {!! Form::label('email', 'Payment:') !!}
        {!! Form::text('payment', null, array('class' => 'form-control', 'required' => '')) !!}
        <!-- Difficulties/injuries -->
        {!! Form::label('d_i', 'Difficulties/Injuries:') !!}
        {!! Form::textarea('d_i', null, array('class' => 'form-control', 'rows' => '4', 'required' => '')) !!}
        <!-- Notes -->
        {!! Form::label('notes', 'Notes:') !!}
        {!! Form::textarea('notes', null, array('class' => 'form-control', 'rows' => '4')) !!}
        <!-- Goals -->
        {!! Form::label('goals', 'Goals:') !!}
        {!! Form::textarea('goals', null, array('class' => 'form-control', 'rows' => '4')) !!}


        <br><br>
        <h4>Emergency</h4>
        <hr>
        <!-- Emergency Contact Person -->
        {!! Form::label('emergency_contact_person', 'Contact Person:') !!}
        {!! Form::text('emergency_contact_person', null, array('class' => 'form-control', 'required' => '')) !!}
        <!-- Emergency Contact Number -->
        {!! Form::label('emergency_contact_number', 'Contact Number:') !!}
        {!! Form::text('emergency_contact_number', null, array('class' => 'form-control', 'required' => '')) !!}

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top:20px">Close</button>
        {!! Form::submit('Add New Customer', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px')) !!}
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>




@include('partials._scripts')
<script>
    jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
  $(function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0",
      dateFormat: 'yy-mm-dd'
    });
  });
  </script>
</body>

</html>

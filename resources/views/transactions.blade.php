<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials._head')
</head>

<body>

    <div id="wrapper">

        @include('partials._navbar')

        <div id="page-wrapper" style="overflow: auto;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Rents & Purchases</h1>
                </div>
            </div>

            <div class="col-lg-9">  
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Type</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Payment</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $transaction)
                        <tr>
                            <td>{{$transaction->customer->first_name}} {{$transaction->customer->last_name}}</td>
                            <td>{{$transaction->type}}</td>
                            <td>{{$transaction->item}}</td>
                            <td>{{$transaction->quantity}}</td>
                            <td>{{$transaction->payment}}</td>
                            <td>{{ date('F d, Y',strtotime($transaction->date)) }}</td>
                            <td>{{ date('g:i A',strtotime($transaction->date)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            
            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Add Transaction
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        {!! Form::open([]) !!}
                        
                        {!! Form::label('item', 'Date:') !!}
                        {!! Form::text('date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required' => '')) !!}
                        
                        {!! Form::label('Transaction_Type', 'Transaction type:') !!}
                        {!! Form::select('transaction_type', ['Rent' => 'Rent', 'Purchase' => 'Purchase'], null, ['placeholder' => 'Choose type...', 'class' => 'form-control', 'required' => '']) !!}

                        {!! Form::label('Customer', 'Customer:') !!}
                        {{ Form::select('customer_id', $fullname, null, array('class' => 'form-control', 'required' => '', 'placeholder' => 'Choose Customer...')) }}

                        {!! Form::label('item', 'Item:') !!}
                        {!! Form::text('item', null, array('class' => 'form-control', 'required' => '')) !!}

                        {!! Form::label('item', 'Quantity:') !!}
                        {!! Form::text('quantity', null, array('class' => 'form-control', 'required' => '')) !!}

                        {!! Form::label('payment', 'Payment:') !!}
                        <div class="form-group input-group">
                            <span class="input-group-addon">₱</span>
                        {!! Form::text('payment', null, array('class' => 'form-control number', 'required' => '')) !!}
                        </div>

                        @if (Auth::guest()) {!! Form::submit('Submit', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px', 'disabled')) !!} @else {!! Form::submit('Submit', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px')) !!} @endif


                        {!! Form::close() !!}
                    </div>
                    <!-- .panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
    @include('partials._scripts')
</body>

</html>

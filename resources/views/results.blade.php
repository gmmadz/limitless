<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials._head')
</head>

<body>

    <div id="wrapper">

        @include('partials._navbar')

        <div id="page-wrapper" style="overflow: auto;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">From <b>{{date('M d, Y', strtotime($from))}}</b> to <b>{{date('M d, Y', strtotime($to))}}</b></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Summary
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Source</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="#tuition">Tuition ({{$tuitions->count()}})</a></td>
                                            <?php $tuition_total = 0;
                                            foreach($tuitions as $tuition){
                                                $tuition_total += $tuition->payment;
                                            }
                                            ?>
                                            <td>{{number_format($tuition_total)}}</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#dropins">Drop-In's ({{$dropins->count()}})</a></td>
                                            <?php $dropin_total = 0;
                                            foreach($dropins as $dropin){
                                                $dropin_total += $dropin->payment;
                                            }
                                            ?>
                                            <td>{{number_format($dropin_total)}}</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#purchases">Purchases ({{$transactions->where('type','=','Purchase')->count()}})</a></td>
                                            <?php $purchases_total = 0;
                                            foreach($transactions as $transaction){
                                                if($transaction->type == "Purchase")
                                                    $purchases_total += $transaction->payment;
                                            }
                                            ?>
                                            <td>{{number_format($purchases_total)}}</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#rents">Rents ({{$transactions->where('type','=','Rent')->count()}})</a></td>
                                            <?php $rents_total = 0;
                                            foreach($transactions as $transaction){
                                                if($transaction->type == "Rent")
                                                    $rents_total += $transaction->payment;
                                            }
                                            ?>
                                            <td>{{number_format($rents_total)}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>TOTAL</b></td>
                                            <td><b>{{number_format($tuition_total+$dropin_total+$purchases_total+$rents_total)}}</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Breakdown
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="tuition">
                                    <thead>
                                        <tr>
                                            <th colspan="4" ><h3><b>TUITION</b></h3></th>
                                        </tr>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Membership</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tuitions as $tuition)
                                        <tr>
                                            <td><a href="/customers/{{$tuition->customer->id}}">{{$tuition->customer->first_name}}</a></td>
                                            <td>{{$tuition->type}}</td>
                                            <td>{{date('F d, Y',strtotime($tuition->date))}}</td>
                                            <td>{{number_format($tuition->payment)}}</td>
                                        </tr>
                                    @endforeach
                                        <tr>
                                            <td colspan="3"><b><center>T U I T I O N &nbsp&nbsp&nbsp&nbsp T O T A L</center></b></td>
                                            <td><b>{{number_format($tuition_total)}}</b></td>
                                        </tr>
                                    </tbody>
                                </table>

                                {{-- DROPINS --}}
                                <table class="table table-bordered table-hover" id="dropins">
                                    <thead>
                                        <tr>
                                            <th colspan="4" ><h3><b>DROP-IN'S</b></h3></th>
                                        </tr>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dropins as $dropin)
                                        <tr>
                                            <td><a href="customers/{{$dropin->customer->id}}">{{$dropin->customer->first_name}}</a></td>
                                            <td>{{date('F d, Y (l)',strtotime($dropin->date))}}</td>
                                            <td>{{date('g:i A   ',strtotime($dropin->date))}}</td>
                                            <td>{{number_format($dropin->payment)}}</td>
                                        </tr>
                                    @endforeach
                                        <tr>
                                            <td colspan="3"><b><center>D R O P - I N ' S &nbsp&nbsp&nbsp&nbsp T O T A L</center></b></td>
                                            <td><b>{{number_format($dropin_total)}}</b></td>
                                        </tr>
                                    </tbody>
                                </table>

                                {{-- PURCHASES --}}
                                <table class="table table-bordered table-hover" id="purchases">
                                    <thead>
                                        <tr>
                                            <th colspan="5" ><h3><b>PURCHASES</b></h3></th>
                                        </tr>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transactions as $purchase)
                                        @if($purchase->type == "Purchase")
                                        <tr>
                                            <td><a href="customers/{{$purchase->customer->id}}">{{$purchase->customer->first_name}}</a></td>
                                            <td>{{$purchase->item}}</td>
                                            <td>{{$purchase->quantity}}</td>
                                            <td>{{date('F d, Y',strtotime($purchase->date))}}</td>
                                            <td>{{number_format($purchase->payment)}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                        <tr>
                                            <td colspan="4"><b><center>P U R C H A S E S &nbsp&nbsp&nbsp&nbsp T O T A L</center></b></td>
                                            <td><b>{{number_format($purchases_total)}}</b></td>
                                        </tr>
                                    </tbody>
                                </table>

                                {{-- RENTS --}}
                                <table class="table table-bordered table-hover" id="rents">
                                    <thead>
                                        <tr>
                                            <th colspan="5" ><h3><b>RENTS</b></h3></th>
                                        </tr>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transactions as $purchase)
                                        @if($purchase->type == "Rent")
                                        <tr>
                                            <td><a href="/customers/{{$purchase->customer->id}}">{{$purchase->customer->first_name}}</a></td>
                                            <td>{{$purchase->item}}</td>
                                            <td>{{$purchase->quantity}}</td>
                                            <td>{{date('F d, Y',strtotime($purchase->date))}}</td>
                                            <td>{{number_format($purchase->payment)}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                        <tr>
                                            <td colspan="4"><b><center>R E N T S &nbsp&nbsp&nbsp&nbsp T O T A L</center></b></td>
                                            <td><b>{{number_format($rents_total)}}</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

    </div>
</div>
@include('partials._scripts')
</body>

</html>

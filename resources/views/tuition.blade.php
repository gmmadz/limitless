<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials._head')
</head>

<body>

    <div id="wrapper">

        @include('partials._navbar')

        <div id="page-wrapper" style="overflow: auto;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tuition Payments</h1>
                </div>
            </div>

            <div class="col-lg-9">  
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Payment</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tuitions as $tuition)
                        <tr>
                            <td>{{$tuition->customer->first_name}} {{$tuition->customer->last_name}}</td>
                            <td>{{$tuition->payment}}</td>
                            <td>{{ date('F d, Y',strtotime($tuition->date)) }}</td>
                            <td>{{ date('g:i A',strtotime($tuition->date)) }}</td>
                            <td>
                            {!! Form::open(['route' => ['tuitions.destroy', $tuition->customer_id], 'method' => 'DELETE']) !!}
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit' ,'class' => 'btn btn-danger']) !!}
                                <input type="hidden" name="tid" value="{{$tuition->id}}" />
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            
            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Tuition Payment
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        {!! Form::open([]) !!}
                        
                        {!! Form::label('item', 'Date:') !!}
                        {!! Form::text('date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required' => '', 'id' => 'date')) !!}
                        
                        {!! Form::label('Customer', 'Customer:') !!}
                        {{ Form::select('customer_id', $fullname, null, array('class' => 'form-control', 'required' => '', 'placeholder' => 'Choose Customer...', 'id' => 'customer')) }}

                        {!! Form::label('Transaction_Type', 'Membership type:') !!}
                        {!! Form::select('type', ['Single' => 'Single Membership', 'Dual' => 'Dual Membership', 'Triple' => 'Triple Membership', 'Quad' => 'Quad Membership'], null, ['placeholder' => 'Choose type...', 'class' => 'form-control', 'required' => '','id' => 'type']) !!}


                        {!! Form::label('payment', 'Payment:') !!}
                        <div class="form-group input-group">
                            <span class="input-group-addon">₱</span>
                            {!! Form::text('payment', null, array('class' => 'form-control number', 'required' => '','id' => 'payment')) !!}
                            <span class="input-group-addon">.00</span>
                        </div>

                        <!-- Confirmation Modal -->
                        <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Confirmation</h3>
                            </div>
                            <div class="modal-body">

                                Are you sure you want to add:<br><br>
                                <label for="male">Date: </label>
                                <input  id="datehere" class="form-control" disabled></input><br>
                                <label for="male">Customer: </label>
                                <input  id="namehere" class="form-control" disabled></input><br>
                                <label for="male">Membership Type: </label>
                                <input  id="typehere" class="form-control" disabled></input><br>
                                <label for="male">Payment: </label>
                                <input  id="paymenthere" class="form-control" disabled></input><br>

                                <br><br>
                                <i>*You cannot edit this once you confirm. Please make sure you entered the right details.</i><br>

                                <div class="modal-footer">
                                    {!! Form::submit('Confirm', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @if (Auth::guest()) <input type="button" name="btn" value="Submit" id="submitBtn" data-toggle="modal" data-target="#confirm" class="btn btn-primary" disabled /> @else <input type="button" name="btn" value="Submit" id="submitBtn" data-toggle="modal" data-target="#confirm" class="btn btn-primary" /> @endif


                {!! Form::close() !!}
            </div>
            <!-- .panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
</div>
@include('partials._scripts')
<script>
    $('#submitBtn').click(function() {
       /* when the button in the form, display the entered values in the modal */
       $('#namehere').val($('#customer').val());
       $('#datehere').val($('#date').val());
       $('#typehere').val($('#type').val());
       $('#paymenthere').val($('#payment').val());
   });
</script>
<body>

    </html>

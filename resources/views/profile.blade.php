<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials._head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
</head>

<body>

    <div id="wrapper">

        @include('partials._navbar')

        <div id="page-wrapper" style="overflow: auto;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{$customer->fullname}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="col-lg-5">
                <!-- PERSONAL INFO -->
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Member Info</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <dl class="">
                                <dt>First name</dt>
                                <dd>{{$customer->first_name}}</dd>
                                
                                <dt>Last name</dt>
                                <dd>{{$customer->last_name}}</dd>

                                <dt>Gender</dt>
                                <dd>{{$customer->gender}}</dd>

                                <dt>Date of Birth</dt>
                                <dd>{{date('F d, Y',strtotime($customer->date_of_birth))}}</dd>
                            </dl>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                <!-- Membership INFO -->
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Membership Details</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <dl class="">
                                <dt>Original Joined Date</dt>
                                <dd>{{date('F d, Y',strtotime($customer->join_date))}}</dd>

                                <dt>Current Joined Date</dt>
                                <dd>{{date('F d, Y',strtotime($customer->current_joined_date))}}</dd>
                                
                                <?php 
                                    $date1 = new DateTime();
                                    $date2 = new DateTime($customer->current_joined_date. '+30 days');

                                    $diff = $date2->diff($date1)->format("%a");
                                ?>

                                <dt>Status</dt>
                                <dd>
                                @if($date1 == $date2)
                                    <span class="label label-default">Inactive</span>
                                @elseif($date1 > $date2)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-success">Active</span>
                                @endif

                                <span class="label label-info">
                                @if($date1 == $date2)
                                    Expired <b>{{$diff}}</b> day/s ago.
                                @elseif($date1 > $date2)
                                    <b>Expires Today!</b>
                                @else
                                    Expires in <b>{{$diff}}</b> day/s.
                                @endif
                                </span>
                                </dd>

                                <dt>Membership Type</dt>
                                <dd>{{$customer->membership_type}} Membership</dd>

                                <dt>Notes</dt>
                                <dd>{{$customer->notes}}</dd>

                                <dt>Goals</dt>
                                <dd>{{$customer->goals}}</dd>

                                <dt>Difficulties/Injuries</dt>
                                <dd>{{$customer->difficulties_injuries}}</dd>
                            </dl>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                <!-- Emergency INFO -->
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Emergency Details</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <dl class="">
                                <dt>Contact Person</dt>
                                <dd>{{$customer->emergency_contact_person}}</dd>

                                <dt>Contact No.</dt>
                                <dd>{{$customer->emergency_contact_number}}</dd>
                            </dl>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                <!-- actions -->
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Actions</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit">Edit</button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete">Delete</button>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

            </div>


            <!-- Edit Customer Modal -->
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel">Edit Member Details</h3>
                </div>
                <div class="modal-body">

                    <h4>Member Info</h4>
                    <hr>
                    {!! Form::model($customer, ['route' => ['customers.update', $customer->id], 'method' => 'PUT']) !!}
                    <input type="hidden" name="date" value="{{\Carbon\Carbon::now()}}"/>

                    <!-- Last Name -->
                    {!! Form::label('last_name', 'Last Name:') !!}
                    {!! Form::text('last_name', $customer->last_name, array('class' => 'form-control', 'required' => '')) !!}
                    <!-- First Name -->
                    {!! Form::label('first_name', 'First Name:') !!}
                    {!! Form::text('first_name', $customer->first_name, array('class' => 'form-control', 'required' => '')) !!}
                    <!-- Gender -->
                    {!! Form::label('Gender', 'Gender:') !!}
                    {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], $customer->gender, ['placeholder' => 'Gender...', 'class' => 'form-control']) !!}
                    <!-- Date of Birth -->
                    {!! Form::label('last_name', 'Birthdate:') !!}
                    {!! Form::text('date_of_birth', $customer->date_of_birth, array('id' => 'datepicker', 'class' => 'form-control')) !!}

                    <!-- Address -->
                    {!! Form::label('address', 'Address:') !!}
                    {!! Form::text('address', $customer->address, array('class' => 'form-control', 'required' => '')) !!}
                    <!-- Phone No. -->
                    {!! Form::label('phone_no', 'Mobile No.:') !!}
                    {!! Form::text('phone_no', $customer->mobile_number, array('class' => 'form-control', 'required' => '')) !!}
                    <!-- Email -->
                    {!! Form::label('email', 'E-Mail:') !!}
                    {!! Form::text('email', $customer->email, array('class' => 'form-control', 'required' => '')) !!}


                    <br><br>
                    <h4>Membership Details</h4>
                    <hr>
                    <!-- Difficulties/injuries -->
                    {!! Form::label('d_i', 'Difficulties/Injuries:') !!}
                    {!! Form::textarea('d_i', $customer->difficulties_injuries, array('class' => 'form-control', 'rows' => '4', 'required' => '')) !!}
                    <!-- Notes -->
                    {!! Form::label('notes', 'Notes:') !!}
                    {!! Form::textarea('notes', $customer->notes, array('class' => 'form-control', 'rows' => '4')) !!}
                    <!-- Goals -->
                    {!! Form::label('goals', 'Goals:') !!}
                    {!! Form::textarea('goals', $customer->goals, array('class' => 'form-control', 'rows' => '4')) !!}


                    <br><br>
                    <h4>Emergency</h4>
                    <hr>
                    <!-- Emergency Contact Person -->
                    {!! Form::label('emergency_contact_person', 'Contact Person:') !!}
                    {!! Form::text('emergency_contact_person', $customer->emergency_contact_person, array('class' => 'form-control', 'required' => '')) !!}
                    <!-- Emergency Contact Number -->
                    {!! Form::label('emergency_contact_number', 'Contact Number:') !!}
                    {!! Form::text('emergency_contact_number', $customer->emergency_contact_number, array('class' => 'form-control', 'required' => '')) !!}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top:20px">Close</button>
                    {!! Form::submit('Save Changes', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <!-- DELETE Modal -->
    {!! Form::open(array('url' => "/delete/{$customer->id}")) !!}
        <input type="hidden" name="id" value="{{$customer->id}}">
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel">Are you sure you want to delete this member?</h3>
                </div>
                <div class="modal-body">
                This member's <b>TRANSACTION</b>, <b>DROP-IN</b> and <b>TUITION</b> records will also be deleted. 
                <br><br>
                The following records wil be deleted: 
                <br><br>
                <i>
                --Transactions: <br>
                {{$customer->transactions}} 
                <br>
                --Tuition: <br>
                {{$customer->tuitions}} 
                <br>
                --Drop-In's: <br>
                {{$customer->dropins}}
                </i>
                <br><br>
                Are you sure you want to delete <b>{{$customer->first_name}} {{$customer->last_name}}</b> and all of his/her records?
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Yes, delete this member and all of his/her records', array('class' => 'btn btn-danger', 'style' => 'margin-top: 20px')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="col-lg-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Payments</b>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" aria-expanded="true">Tuition</a>
                    </li>
                    <li class=""><a href="#profile" data-toggle="tab" aria-expanded="false">Drop-In's</a>
                    </li>
                    <li class=""><a href="#messages" data-toggle="tab" aria-expanded="false">Purchases</a>
                    </li>
                    <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Rents</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="home">
                        <h4>Tuition Payment History</h4>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                         <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Membership</th>
                                    <th>Payment</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer->tuitions as $tuition)
                                <tr>
                                    <td>{{date('F d, Y',strtotime($tuition->date))}}</td>
                                    <td>{{$tuition->type}}</td>
                                    <td>{{$tuition->payment}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <div class="tab-pane fade" id="profile">
                    <h4>Drop-In History</h4>
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-3">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Payment</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer->dropins as $dropin)
                                <tr>
                                    <td>{{date('F d, Y (l)',strtotime($dropin->date))}}</td>
                                    <td>{{date('g:i A',strtotime($dropin->date))}}</td>
                                    <td>₱ {{$dropin->payment}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="messages">
                    <h4>Purchases</h4>
                    <!-- Purchases -->
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-1">
                            <thead>
                             <tr>
                                <th>Type</th>
                                <th>Item</th>
                                <th>Payment</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customer->transactions as $transaction)
                            @if($transaction->type == "Purchase")
                            <tr>
                                <td>{{$transaction->type}}</td>
                                <td>{{$transaction->item}}</td>
                                <td>{{$transaction->payment}}</td>
                                <td>{{date('F d, Y',strtotime($transaction->date))}}</td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>

            <div class="tab-pane fade" id="settings">
                <h4>Rents</h4>
                <!-- rents -->

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-2">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Item</th>
                                <th>Payment</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customer->transactions as $transaction)
                            @if($transaction->type == "Rent")
                            <tr>
                                <td>{{$transaction->type}}</td>
                                <td>{{$transaction->item}}</td>
                                <td>{{$transaction->payment}}</td>
                                <td>{{date('F d, Y',strtotime($transaction->date))}}</td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>

                    <!-- /.panel -->
                </div>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>

@include('partials._scripts')
<script>
    $(document).ready(function() {
        $('#dataTables-1').DataTable({
            responsive: true
        });
    });

    $(document).ready(function() {
        $('#dataTables-2').DataTable({
            responsive: true
        });
    });

    $(document).ready(function() {
        $('#dataTables-3').DataTable({
            responsive: true
        });
    });
</script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
  $(function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0",
      dateFormat: 'yy-mm-dd'
    });
  });
  </script>
</body>

</html>
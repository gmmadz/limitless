<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials._head')
</head>

<body>

    <div id="wrapper">

        @include('partials._navbar')

        <div id="page-wrapper" style="overflow: auto;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Drop-In's</h1>
                </div>
            </div>

            <div class="col-lg-9">  
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Payment</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dropins as $dropin)
                        <tr>
                            <td>{{$dropin->customer->first_name}} {{$dropin->customer->last_name}}</td>
                            <td>{{$dropin->payment}}</td>
                            <td>{{ date('F d, Y (l)',strtotime($dropin->date)) }}</td>
                            <td>{{ date('g:i A',strtotime($dropin->date)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            
            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Register Drop-in
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        {!! Form::open([]) !!}

                        {!! Form::label('item', 'Date:') !!}
                        {!! Form::text('date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required' => '')) !!}

                        {!! Form::label('Customer', 'Customer:') !!}
                        {{ Form::select('customer_id', $fullname, null, array('class' => 'form-control', 'required' => '', 'placeholder' => 'Choose Customer...')) }}

                        {!! Form::label('payment', 'Payment:') !!}
                        
                        <div class="form-group input-group">
                            <span class="input-group-addon">₱</span>
                            {!! Form::text('payment', 50, array('class' => 'form-control number', 'required' => '')) !!}
                            <span class="input-group-addon">.00</span>
                        </div>

                        

                        @if (Auth::guest()) {!! Form::submit('Register', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px', 'disabled')) !!} @else {!! Form::submit('Register', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px')) !!} @endif


                        {!! Form::close() !!}
                    </div>
                    <!-- .panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
    @include('partials._scripts')
</body>

</html>
